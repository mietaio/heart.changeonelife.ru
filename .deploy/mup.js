module.exports = {
	servers: {
		one: {
			host: '192.81.221.25',
			username: 'ops',
			opts: {
				port: 2256
			}
		}
	},

	meteor: {
		name: 'heart',
		path: '../',
		servers: {
			one: {}
		},

		buildOptions: {
			serverOnly: false,
			debug: false,
		},

		env: {
			PORT: 3000,
			ROOT_URL: 'http://heart.changeonelife.ru/',
			MONGO_URL: 'mongodb://localhost/meteor'
		},

		dockerImage: 'abernix/meteord:base',
		enableUploadProgressBar: true,
		deployCheckWaitTime: 120
  },

	mongo: {
		oplog: true,
		port: 27017,
		servers: {
			one: {},
		},
	},
};
