Template.registerHelper('shareOnFacebookLink', () => {
    return 'https://www.facebook.com/sharer/sharer.php?u=https://heart.changeonelife.ru'
})

Template.registerHelper('shareOnVkontakteLink', () => {
    return 'https://vk.com/share.php?url=https://heart.changeonelife.ru'
})

Template.registerHelper('shareOnOdnoklassnikiLink', () => {
    return 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=https://heart.changeonelife.ru'
})
