import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

export const Files = new Mongo.Collection('files')
