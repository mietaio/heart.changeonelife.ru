import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

export const Videos = new Mongo.Collection('videos')
