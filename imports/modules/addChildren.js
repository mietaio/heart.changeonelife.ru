import $ from 'jquery'
import 'jquery-validation'
import { Meteor } from 'meteor/meteor'
import { Bert } from 'meteor/themeteorchef:bert'

export const validate = () => {
	$('form').validate({
		rules: {
			name: {
				required: true,
			},
			location: {
				required: true,
			},
			url: {
				required: true,
			},
			image: {
				required: true,
			},
		},
		messages: {
			name: {
				required: 'Введите имя',
			},
			location: {
				required: 'Введите локацию'
			},
			url: {
				required: 'Введите url'
			},
			image: {
				required: 'Выберите файл'
			},
		},
		submitHandler() {
			addChildren()
		}
	})
}

const addChildren = () => {
	const children = {
        name: $('[name=name]').val(),
        location: $('[name=location]').val(),
        url: $('[name=url]').val(),
		pulse: 'pulse_sm.svg',
		image: $('input[type="file"]').attr('data-url')
    }

	Meteor.call('children.create', children, (error, result) => {
		if (result) {
			Bert.alert( 'Запись успешно добавлена', 'success', 'fixed-top', 'fa-check' )
			FlowRouter.redirect('/admin/')
		}
	})
}
