import sockets from './sockets'
import headtrackr from './headtrackr'
import { mean, normalize, frequencyExtract, filterFreq } from './math'
import { Childrens } from '/imports/api/childrens'

export default camera = (() => {

    const fps = 15, bufferWindow = 512

    let beatsArray = []
    let sendingData = false
    let red = []
    let green = []
    let blue = []
    let htracker
    let video, canvas, context, canvasOverlay, contextOverlay
    let pause = false
    let renderTimer, dataSend, workingBuffer, heartbeatTimer
    let width = 380, height = 285
    let localStream
    let spectrum
    let heartrate

    function initVideoStream() {
        video = document.createElement('video')
        video.setAttribute('height', height)
        video.setAttribute('height', height)

        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia

        window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL

        if (navigator.getUserMedia) {

            navigator.getUserMedia({
                video: true,
                audio: false
            }, (stream) => {

                localStream = stream
                video.src = window.URL.createObjectURL(stream)
                initCanvas()

            }, errorCallback)
        }
    }

    function initCanvas() {
        canvas = document.createElement('canvas')
        canvas.setAttribute('width', width)
        canvas.setAttribute('height', height)
        canvas.setAttribute('id', 'canvas')

        canvas.style.position = 'absolute'
        // document.body.appendChild(canvas)

        context = canvas.getContext('2d')

        canvasOverlay = document.createElement("canvas")
        canvasOverlay.setAttribute("width", width)
        canvasOverlay.setAttribute("height", height)
        canvasOverlay.setAttribute('id', 'canvasOverlay')

        canvasOverlay.style.position = 'absolute'
        // document.body.appendChild(canvasOverlay)

        contextOverlay = canvasOverlay.getContext("2d")
        contextOverlay.clearRect(0, 0, width, height)

        startCapture()
    }

    function startCapture() {

        video.play()
        sockets.open()

        renderTimer = setInterval(() => {
            context.drawImage(video, 0, 0, width, height)
        }, Math.round(1000 / fps))

        dataSend = setInterval(function(){

            if (sendingData){
                sockets.sendData(JSON.stringify({"array": [red, green, blue], "bufferWindow": green.length}))
            }

        }, Math.round(1000))

        headtrack()
    }

    function headtrack() {

        htracker = new headtrackr.Tracker({ui: false, detectionInterval: 1000/fps})
        htracker.init(video, canvas, context)
        htracker.start()

        document.addEventListener('headtrackrStatus', trackStatus)
        document.addEventListener('facetrackingEvent', greenRect)
    }

    function trackStatus(event) {
        console.log(event.status)

        if (event.status == 'redetecting') {
            for (var i = 1; i < 99999; i++)
                window.clearInterval(i)

            const video = document.getElementById('main-video')
            video.pause()

            $('.lost-popup').css({display: 'block'})
            $('.lost-popup .content').velocity("transition.slideDownIn", 300)
            $('.lost-popup .overlay').velocity({opacity: 0.5})
        }
    }

    function greenRect(event) {

        contextOverlay.clearRect(0,0,width,height)

        var sx, sy, sw, sh, forehead, inpos, outpos
        var greenSum = 0
        var redSum = 0
        var blueSum = 0

        sx = event.x + (-(event.width/5)) + 20 >> 0
        sy = event.y + (-(event.height/3)) + 10 >> 0
        sw = (event.width/5) >> 0
        sh = (event.height/10) >> 0

        if (event.detection == "CS") {

            contextOverlay.rotate(event.angle-(Math.PI/2))
            contextOverlay.strokeStyle = "#00CC00"
            contextOverlay.strokeRect(event.x + (-(event.width/2)) >> 0, event.y + (-(event.height/2)) >> 0, event.width, event.height)
            contextOverlay.strokeStyle = "#33CCFF";
            contextOverlay.strokeRect(sx, sy, sw, sh)

            forehead = context.getImageData(sx, sy, sw, sh)

            for (i = 0; i < forehead.data.length; i+=4){
                redSum = forehead.data[i] + redSum
                greenSum = forehead.data[i+1] + greenSum
                blueSum = forehead.data[i+2] + blueSum
            }

            var redAverage = redSum/(forehead.data.length/4)
            var greenAverage = greenSum/(forehead.data.length/4)
            var blueAverage = blueSum/(forehead.data.length/4)

            if (green.length < bufferWindow){
                red.push(redAverage)
                green.push(greenAverage)
                blue.push(blueAverage)

              if (green.length > bufferWindow/8){
                  sendingData = true
              }

            } else {
                red.push(redAverage)
                red.shift()
                green.push(greenAverage)
                green.shift()
                blue.push(blueAverage)
                blue.shift()
            }

            contextOverlay.rotate((Math.PI/2)-event.angle)
        }
    }

    function cardiac(array, bfwindow) {
        spectrum = array
        var freqs = frequencyExtract(spectrum, fps)
        var freq = freqs.freq_in_hertz
        heartrate = freq * 60
        beatsArray.push(~~heartrate)
        Session.set('heartrate', mean(beatsArray))
    }

    function pauseCapture(){
        var track = localStream.getTracks()[0]
        track.stop()

        if (renderTimer) clearInterval(renderTimer)
        if (dataSend) clearInterval(dataSend)
        if (heartbeatTimer) clearInterval(heartbeatTimer)

        pause = true
        sendingData = false
        video.pause()

        htracker.stop()
        sockets.close()
        document.removeEventListener("facetrackingEvent", greenRect)
        document.removeEventListener("headtrackrStatus", trackStatus)

    };

    const errorCallback = (error) => {
        BlazeLayout.render('error')
    }

    return {
        initVideoStream() {
            initVideoStream()
        },
        startCanvas() {
            initCanvas()
        },
        cardiac: cardiac,
        pause: pauseCapture,
    }

})()
