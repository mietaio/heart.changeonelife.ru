import $ from 'jquery'
import 'jquery-validation'
import { Meteor } from 'meteor/meteor'
import { Bert } from 'meteor/themeteorchef:bert'

export const validate = () => {
	$('form').validate({
		rules: {
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
			}
		},
		messages: {
			email: {
				required: 'Введите email',
				email: 'Вы ввели неверный email'
			},
			password: {
				required: 'Введите пароль'
			}
		},
		submitHandler() {
			login()
		}
	})
}

const login = () => {
	const email = $('[name=email]').val()
	const password = $('[name=password]').val()

	Meteor.loginWithPassword(email, password, (error) => {
		if (error) {
			if (error.reason === 'Incorrect password') {
				Bert.alert( 'Неверный пароль.', 'danger', 'fixed-top', 'fa-frown-o' )
			} else if (error.reason === 'User not found') {
				Bert.alert( 'Пользователь с таким email не зарегистрирован.', 'danger', 'fixed-top', 'fa-frown-o' )
			}
		}
	})
}
