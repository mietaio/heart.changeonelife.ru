import camera from './camera'

export default sockets = {
    open() {
        dataSocket = new WebSocket('wss://heart.changeonelife.ru:8000/echo')

        dataSocket.onopen = function(){
            console.log("websocket opened!")
        }

        dataSocket.onclose = function(){
            console.log('websocket closed!')
        },

        dataSocket.onmessage =  function(e){
            var data = JSON.parse(e.data)

            if (data.id === "ICA"){
                camera.cardiac(data.array, data.bufferWindow)
            }
        }
    },

    close() {
        dataSocket.close()
    },

    sendData(data) {
        dataSocket.send(data)
    }
}
