import { FlowRouter } from 'meteor/kadira:flow-router'
import { BlazeLayout } from 'meteor/kadira:blaze-layout'

import '/imports/ui/layouts/default'
import '/imports/ui/layouts/finished'

import '/imports/ui/pages/welcome/welcome'
import '/imports/ui/pages/result/result'

import '/imports/ui/pages/admin/admin'
import '/imports/ui/pages/addChildren/addChildren'
import '/imports/ui/pages/addVideo/addVideo'

import '/imports/ui/components/login/login'
import '/imports/ui/components/adminPanel/adminPanel'

BlazeLayout.setRoot('body')

FlowRouter.route( '/', {
    action() {
        BlazeLayout.render('welcome')
    }
})

FlowRouter.route( '/admin/', {
	name: 'admin',
    action() {
        BlazeLayout.render('admin')
    }
})

FlowRouter.route( '/admin/add/children', {
    action() {
        BlazeLayout.render('addChildren')
    }
})

FlowRouter.route( '/admin/add/video', {
    action() {
        BlazeLayout.render('addVideo')
    }
})

FlowRouter.notFound = {
    action() {
        FlowRouter.go('/')
    }
}

FlowRouter.route( '/:_id', {
    triggersEnter: [trackRouteEntry],
    action(params) {
        BlazeLayout.render('finished', {yield: 'result'})
    }
})

function trackRouteEntry(context) {
    const type = typeof Persons
    if (type === 'undefined') FlowRouter.go('/')
}
