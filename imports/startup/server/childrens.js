import { Meteor } from 'meteor/meteor'
import { Childrens } from '/imports/api/childrens'

Meteor.publish('childrens', function () {
    const selector = {}
    const options = {
        fields: {_id: 1, number: 1, name: 1, location: 1, image: 1, url: 1, pulse: 1},
        sort: {createdAt: -1},
    }

    return Childrens.find(selector, options)
})
