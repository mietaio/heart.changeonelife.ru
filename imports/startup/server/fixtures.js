import { Meteor } from 'meteor/meteor'
import { Roles } from 'meteor/alanning:roles'
import { Accounts } from 'meteor/accounts-base'

const users = [
    {
        email: 'hello@mieta.io',
        password: 'CK4pNHNcGz4Z4yitBX4Q2Q==',
        roles: ['admin'],
    },
    {
        email: 'vladimir.Bulanov@ddb.ru',
        password: 'oVBhdqgzYHgWyYtrJu3xrw==',
        roles: ['admin'],
    },
    {
        email: 'irina.zhabura@ddb.ru',
        password: 'wMSrTmnUWrNhKXbqLBmBmQ==',
        roles: ['admin'],
    },
]

users.forEach(({ email, password, profile, roles }) => {
    const userExists = Meteor.users.findOne({ 'emails.address': email })

    if (!userExists) {
        const userId = Accounts.createUser({ email, password, profile })
		Roles.addUsersToRoles(userId, roles)
    }
})
