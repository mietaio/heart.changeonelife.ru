import { Meteor } from 'meteor/meteor'
import { Childrens } from '/imports/api/childrens'
import { Files } from '/imports/api/files'
import { Videos } from '/imports/api/videos'

Meteor.methods({
	'children.create'(children) {
        try {
            const id = Childrens.insert(children)
            return id
        }

        catch(exception) {
            return exception
        }
    },

	'children.delete'(userId) {
        Childrens.remove(userId)
	},

	'videos.remove'() {
		Videos.remove({})
	},

	'videos.add'(video) {
		try {
            const id = Videos.insert(video)
            return id
        }

        catch(exception) {
            return exception
        }
	},

	'file.store'(url) {
        Files.insert(url)
    },
})
