Slingshot.fileRestrictions( "uploadToAmazonS3", {
    allowedFileTypes: [ "image/png", "image/jpeg", "video/mp4" ],
    maxSize: 10 * 1024 * 1024
})

Slingshot.createDirective( "uploadToAmazonS3", Slingshot.S3Storage, {
    bucket: "changeonelife.ru",
    acl: "public-read",
    authorize() {
        return true
    },
    key(file, metacontext) {
		// if (metacontext) file.name = metacontext.fileName
		return Date.now() + file.name
    }
})
