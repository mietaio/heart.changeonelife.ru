import { Meteor } from 'meteor/meteor'
import { Videos } from '/imports/api/videos'

Meteor.publish('videos', function () {
    const selector = {}
    const options = {}

    return Videos.find(selector, options)
})
