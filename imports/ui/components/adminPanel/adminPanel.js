import './adminPanel.jade'
import { Session } from 'meteor/session'
import { Childrens } from '/imports/api/childrens'

Template.adminPanel.onCreated(() => {
    Meteor.subscribe('childrens')
})

Template.adminPanel.helpers({
    childrens() {
        return Childrens.find()
    }
})

Template.adminPanel.events({
	'click .logout'() {
		Meteor.logout()
	},

	'click .delete-button'(event) {
        event.preventDefault()

        const isDelete = confirm("Вы уверены?")

        if(isDelete) {
            Meteor.call('children.delete', this._id, (error) => {
                if(!error) Bert.alert('Запись успешно удалена', 'success', 'fixed-top')
            })
        }
    },
})
