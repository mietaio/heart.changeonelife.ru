import './login.jade'
import { validate } from '/imports/modules/login'

Template.login.onRendered( () => {
	validate()
})

Template.login.events({
	'submit form'(event) {
		event.preventDefault()
	}
})
