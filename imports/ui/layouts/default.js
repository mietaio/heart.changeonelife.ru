import './default.jade'

import '/imports/ui/pages/prepare/prepare'
import '/imports/ui/pages/error/error'
import '/imports/ui/pages/check/check'

import camera from '/imports/modules/camera'

import { Videos } from '/imports/api/videos'

Template.body.onCreated(function() {
	this.autorun(() => {
	    this.subscribe('videos')
  	})
})

Template.body.helpers({
	videos() {
		const videos = Videos.findOne({type: 'pulseVideo'})
		return videos
	}
})

Template.body.onRendered( () => {
	$(window).on('keydown', function(e){
        if (e.which === 27) {
			$('.about-popup .content').velocity("transition.slideDownOut", 300, function() {
				$('.about-popup .overlay').velocity({opacity: 0}, function() {
					$('.about-popup').css({display: 'none'})
				})
			})
		}
    })

	const url = FlowRouter.current().route.name
	$('body').addClass(url)
})

Template.body.events({
    'click .click-area'() {
        $('.about-popup').css({display: 'block'})
        $('.about-popup .content').velocity("transition.slideDownIn", 300)
        $('.about-popup .overlay').velocity({opacity: 0.5})
    },

    'click .about-popup .content-close'() {
        $('.about-popup .content').velocity("transition.slideDownOut", 300, function() {
			$('.about-popup .overlay').velocity({opacity: 0}, function() {
				$('.about-popup').css({display: 'none'})
			})
		})
    },

    'click .browsers-popup .content-close'() {
        $('.browsers-popup .content').velocity("transition.slideDownOut", 300, function() {
            $('.browsers-popup .overlay').velocity({opacity: 0}, function() {
                $('.browsers-popup').css({display: 'none'})
            })
        })
    },

	'click'(event) {
		if ( $(event.target).attr('class') === 'overlay' ) {
			$('.about-popup .content').velocity("transition.slideDownOut", 300, function() {
				$('.about-popup .overlay').velocity({opacity: 0}, function() {
					$('.about-popup').css({display: 'none'})
				})
			})
		}
	},

    'click .lost-popup .startOver'() {
        location.reload()
    }
})
