import './finished.jade'
import '/imports/ui/pages/result/result'

Template.finished.events({
    'click .click-area'() {
        $('.about-popup').css({display: 'block'})
        $('.about-popup .content').velocity("transition.slideDownIn", 300)
        $('.about-popup .overlay').velocity({opacity: 0.5})
    },

    'click .about-popup .content-close'() {
        $('.about-popup .content').velocity("transition.slideDownOut", 300, function() {
			$('.about-popup .overlay').velocity({opacity: 0}, function() {
				$('.about-popup').css({display: 'none'})
			})
		})
    },
})
