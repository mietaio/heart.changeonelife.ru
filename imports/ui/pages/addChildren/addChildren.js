import './addChildren.jade'
import { validate } from '/imports/modules/addChildren'

Template.addChildren.onRendered(() => {
	$('.about-popup').remove()
	$('.browsers-popup').remove()
	$('.lost-popup').remove()
	$('.click-area').remove()
	$('.content').remove()
	$('.change-one-life-logo').remove()
	$('#bg-main-video').remove()
})

Template.addChildren.onRendered( () => {
	validate()
})

Template.addChildren.events({
	'submit form'(event) {
		event.preventDefault()
	},

	'change input[type="file"]'(event, template) {
        const uploader = new Slingshot.Upload("uploadToAmazonS3")

        // $('.progress').fadeIn(() => {
        //     slingshot.set(uploader)
        // })
		//
        uploader.send(event.target.files[0], (error, url) => {
            if (error) {
                console.error('Error uploading', uploader.xhr.response)
                console.log(error)
                Bert.alert('Загрузка не удалась, попробуйте еще раз', 'danger', 'fixed-top', 'fa-frown-o' )
            }
            else {
                Meteor.call('file.store', url)
                Bert.alert('Файл успешно загружен', 'success', 'fixed-top', 'fa-check' )
				$('input[type="file"]').attr('data-url', url)
                // $('.progress').fadeOut(() => {
                //     slingshot.set()
                // })
            }
        })
    },
})
