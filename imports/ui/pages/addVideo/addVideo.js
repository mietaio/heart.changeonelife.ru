import './addVideo.jade'

Template.addVideo.onRendered(() => {
	$('.about-popup').remove()
	$('.browsers-popup').remove()
	$('.lost-popup').remove()
	$('.click-area').remove()
	$('.content').remove()
	$('.change-one-life-logo').remove()
	$('#bg-main-video').remove()
})

Template.addVideo.events({
	'submit form'(event) {
		event.preventDefault()
		const video = {
			type: 'pulseVideo',
			url: $('input[type="file"]').attr('data-url')
		}

		Meteor.call('videos.remove')
		Meteor.call('videos.add', video, (error, result) => {
			if (result) {
				Bert.alert( 'Видео успешно добавлено', 'success', 'fixed-top', 'fa-check' )
				FlowRouter.redirect('/admin/')
			}
		})
	},

	'change input[type="file"]'(event, template) {
		const metacontext = {fileName: "heart.changeonelife.ru.mp4"}
        const uploader = new Slingshot.Upload("uploadToAmazonS3", metacontext)
		const file = event.target.files[0]

        // $('.progress').fadeIn(() => {
        //     slingshot.set(uploader)
        // })
		//
        uploader.send(file, (error, url) => {
            if (error) {
				if (error.error === 'Upload denied') {
					Bert.alert('Загрузка не удалась, неверный формат файла', 'danger', 'fixed-top', 'fa-frown-o')
				}
                // Bert.alert('Загрузка не удалась, попробуйте еще раз', 'danger', 'fixed-top', 'fa-frown-o' )
            }

            else {
                Meteor.call('file.store', url)
                Bert.alert('Файл успешно загружен', 'success', 'fixed-top', 'fa-check' )
				$('input[type="file"]').attr('data-url', url)
                // $('.progress').fadeOut(() => {
                //     slingshot.set()
                // })
            }
        })
    },
})
