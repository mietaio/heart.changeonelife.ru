import './admin.jade'

Template.admin.onRendered(() => {
	$('.about-popup').remove()
	$('.browsers-popup').remove()
	$('.lost-popup').remove()
	$('.click-area').remove()
	$('.content').remove()
	$('.change-one-life-logo').remove()
	$('#bg-main-video').remove()
})
