import './check.jade'
import Mediator from '/imports/modules/mediator'
import { Childrens } from '/imports/api/childrens'

Template.check.helpers({
    percentage() {
        return Session.get('percentage')
    },

    heartrate() {
        const heartrate = Session.get('heartrate')
        return ~~heartrate
    }
})

Template.check.onCreated(() => {
    Meteor.subscribe('childrens')
})

Template.check.onRendered(() => {
    camera.initVideoStream()

    animatedSteps()
    sessionLoadingInterval()

    setInterval(animatedSteps, 10000)

    function animatedSteps() {
        setTimeout(() => {
            $('.check-step').animate({'left': '-50px', opacity: 0}, 250, "linear", function() {
                $('.check-step').animate({'left': '50px'}, 1, function() {
                    $('.check-step__icon').attr('src', 'images/icons/icon-check-2.svg')
                    $('.check-step__text').text('В помещении вокруг вас хорошее освещение')
                    $('.check-step').animate({'left': 0, opacity: 1}, 250, "linear")
                })

            })
        }, 3500)

        setTimeout(() => {
            $('.check-step').animate({'left': '-50px', opacity: 0}, 250, "linear", function() {
                $('.check-step').animate({'left': '50px'}, 1, function() {
                    $('.check-step__icon').attr('src', 'images/icons/icon-check-3.svg')
                    $('.check-step__text').text('В объектив попадает только один человек')
                    $('.check-step').animate({'left': 0, opacity: 1}, 250, "linear")
                })
            })
        }, 7000)

        setTimeout(() => {
            $('.check-step').animate({'left': '-50px', opacity: 0}, 250, "linear", function() {
                $('.check-step').animate({'left': '50px'}, 1, function() {
                    $('.check-step__icon').attr('src', 'images/icons/icon-check-1.svg')
                    $('.check-step__text').text('Для наилучшего результата сядьте поближе к камере')
                    $('.check-step').animate({'left': 0, opacity: 1}, 250, "linear")
                })
            })
        }, 10000)
    }

    function sessionLoadingInterval() {
        let i = 1
        Session.set('percentage', 0)

        var timerId = setInterval(function() {
            Session.set('percentage', i)
            const bPos = `${-i * 98}px`
            $('.check-step__progress-icon').css('background-position', bPos)
            if (i == 100) {
                clearInterval(timerId)

                $('.check').remove()
                $('.heartrate').css('display', 'block')
                $('.measure-top').css('display', 'block')
                $('.measure-bottom').css('display', 'block')
                $('#progress').css('display', 'block')

                const video = document.getElementById('main-video')
                video.style.visibility = 'visible'

                const progress = document.getElementById('progress')
                const progressBar = document.getElementById('progress-bar')

                video.play()

                video.addEventListener('loadedmetadata', function() {
                    progress.setAttribute('max', video.duration)
                })

                video.addEventListener('timeupdate', function() {
                   progress.value = video.currentTime
                   progressBar.style.width = Math.floor((video.currentTime / video.duration) * 100) + '%'
                })

                video.onended = function(e) {
                    camera.pause()
                    video.style.visibility = 'hidden'

                    const childrens = Childrens.find().fetch()

                    // Create local collection
                    Persons = new Mongo.Collection(null)

                    // Insert documents to local collection
                    _.each(childrens, (children) => {
                        Persons.insert(children)
                    })

                    const persons = Persons.find().fetch()

                    // Choice random from all array
                    const random = Random.choice(persons)

                    // Update the image in local collection
                    Persons.update(random._id, {
                        $set: {pulse: 'pulse_b.svg', highlighted: 'highlighted'},
                    })

                    FlowRouter.go(`/${random._id}`)
                }
            }
            i++
        }, 100)
    }
})

Template.check.onDestroyed(() => {
    for (var i = 1; i < 99999; i++)
        window.clearInterval(i)
})
