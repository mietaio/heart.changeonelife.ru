import './error.jade'

Template.error.events({
    'click .goIndex'() {
        location.reload()
    }
})
