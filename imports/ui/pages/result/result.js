import './result.jade'

Template.result.helpers({
    children() {
        return Persons.findOne({_id: FlowRouter.getParam('_id')})
    },

    childrens() {
      return Persons.find()
    },

    heartrate() {
        const heartrate = Session.get('heartrate')
        return ~~heartrate
    },
})

Template.result.events({
    'click .small-heart-logo'() {
        location.reload()
    }
})
