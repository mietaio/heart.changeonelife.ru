import './welcome.jade'
// http://codepen.io/rachsmith/post/how-to-move-elements-on-scroll-in-a-way-that-doesn-t-suck-too-bad

require('jquery-mousewheel')($)
const bowser = require('bowser')

let step = 1
const delay = 40

Template.welcome.onRendered(() => {
	$('.welcome')
		.on('mousewheel', _.debounce(function(event) {
			
			if (step == 1 && event.deltaY < 0) {
				$(this).attr('class', 'welcome step1down')
				$('.pc-icon').addClass('animated')

				clearTimeout($.data(this, 'timer'))
	    		$.data(this, 'timer', setTimeout(function() {
					step = 2
	    		}, delay))
			}
			if (step == 2 && event.deltaY > 0) {
				$(this).attr('class', 'welcome step2up')

				clearTimeout($.data(this, 'timer'))
	    		$.data(this, 'timer', setTimeout(function() {
					step = 1
	    		}, delay))
			}
			if (step == 2 && event.deltaY < 0) {
				$(this).attr('class', 'welcome step2down')
				$('.angel-icon').addClass('animated')

				clearTimeout($.data(this, 'timer'))
	    		$.data(this, 'timer', setTimeout(function() {
					step = 3
	    		}, delay))
			}
			if (step == 3 && event.deltaY > 0) {
				$(this).attr('class', 'welcome step3up')

				clearTimeout($.data(this, 'timer'))
	    		$.data(this, 'timer', setTimeout(function() {
					step = 2
	    		}, delay))
			}
		}, delay, true))
})

Template.welcome.events({
    'click .slide1'() {
		$('.welcome').attr('class', 'welcome step1down')
		step = 2
    },

    'click .slide2'() {
		$('.welcome').attr('class', 'welcome step2down')
		step = 3
    },

    'click .goNext'() {
        if (bowser.safari || bowser.msedge || bowser.msie) {
            $('.browsers-popup').css({display: 'block'})
            $('.browsers-popup .content').velocity("transition.slideDownIn", 300)
            $('.browsers-popup .overlay').velocity({opacity: 0.5})
        }

        else BlazeLayout.render('prepare')
    }
})
